package com.arkon.mbus.locator.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;
/**
 * Spring data default repository
 */
public interface MayoraltyRepository extends MongoRepository<Mayoralty, String>  {

	@Query(value="{'key': ?0}")
	public List<Mayoralty> findByKey(Integer key);
	@Query(value="{'key': {$in:[?0]} }")
	public Set<Mayoralty> findAllMayoraltiesIn(List<Integer> keys);
	
}
