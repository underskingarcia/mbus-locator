package com.arkon.mbus.locator.task;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.arkon.mbus.locator.documents.Record;
import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;
import com.arkon.mbus.locator.services.RecordService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
class ScheduledTasksTest {

	private static final Logger LOG = LoggerFactory.getLogger(ScheduledTasksTest.class);
	
	@Autowired
	private RecordService recordService;
	
	@Test
	@Order(1)
	void testGetPagedData() throws IOException {
		ScheduledTasks task = mock(ScheduledTasks.class);
		assertNotNull(recordService);
		assertNotNull(task);
		task.getPagedData();
		List<Record> testRecords = jsonToRecordList(getTestRecords());
		when(task.getPagedData()).thenReturn(testRecords);
		LOG.info("Found this records: {}", testRecords.size());
		assertNotNull(testRecords);
		assertTrue(testRecords.get(0) instanceof Record);
		verify(task).getPagedData();
	}
	
	@Test
	void testGetMayoralty() {
		ScheduledTasks task = mock(ScheduledTasks.class);
		assertNotNull(recordService);
		assertNotNull(task);
		Mayoralty mayoralty = new Mayoralty();
		mayoralty.setKey(3);
		Integer key = 3;
		when(task.getMayoralty(mayoralty.getName())).thenReturn(mayoralty);
		task.getMayoralty("Azcapotzalco");
		LOG.info("Found this mayoralty: {}", key);
		assertEquals(3, key);
		verify(task).getMayoralty("Azcapotzalco");
	}
	
	private String getTestRecords() throws IOException, JsonParseException, JsonMappingException {
		Path resourceDirectory = Paths.get("src","test","resources", "test_data_metrobus.json");
		String absolutePath = resourceDirectory.toFile().getAbsolutePath();
		ObjectMapper mapper = new ObjectMapper();
		File file = new File(absolutePath);
		JsonNode masterJSON = mapper.readTree(file);
		return masterJSON.toString();
	}

	private List<Record> jsonToRecordList(String response) throws JsonProcessingException, JsonMappingException {
		ObjectMapper jsonMapper = new ObjectMapper();
		return jsonMapper.readValue(response,
				jsonMapper.getTypeFactory().constructCollectionType(List.class, Record.class));
	}
	
}
