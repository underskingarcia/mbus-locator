package com.arkon.mbus.locator.documents.api.geocoding;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class that represent Geocoding Root 0bject records
 */
public class LocationMap {
	
	@JsonProperty("plus_code")
	private PlusCode plusCode;
	private List<Results> results;
	
	public LocationMap() {}

	public PlusCode getPlusCode() {
		return plusCode;
	}

	public void setPlusCode(PlusCode plusCode) {
		this.plusCode = plusCode;
	}

	public List<Results> getResults() {
		return results;
	}

	public void setResults(List<Results> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return "Map [plusCode=" + plusCode + ", results=" + results + "]";
	}

}
