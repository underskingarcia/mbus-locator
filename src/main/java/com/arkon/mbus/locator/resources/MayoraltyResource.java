package com.arkon.mbus.locator.resources;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;
import com.arkon.mbus.locator.exception.RecordException;
import com.arkon.mbus.locator.services.MayoraltyService;
/**
 *This class expose a resource to consume mayoralty collection
 */
@RestController
public class MayoraltyResource {

	@Autowired
	private MayoraltyService service;
	
	@GetMapping("/api/mayoralty")
	public Set<Mayoralty> findAll() {
		return new HashSet<>(service.findAll());
	}
	
	/**
	 * Search all distinct names of mayoralties in records
	 * @return List<String>
	 */
	@GetMapping("/api/mayoralty/available")
	public Set<Mayoralty> findAvailableMayoralties(){
		return service.findAvailableMayoralties();
	}
	
	
}
