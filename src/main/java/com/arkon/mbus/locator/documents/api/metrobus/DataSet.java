package com.arkon.mbus.locator.documents.api.metrobus;

import java.util.List;

import com.arkon.mbus.locator.documents.Record;
/**
 * Class that represent MBus Root 0bject records
 *
 */
public class DataSet {

	private Integer nhits;
	private Parameters parameters;
	private List<Record> records;
	
	public DataSet() {}
	
	public Integer getNhits() {
		return nhits;
	}
	public void setNhits(Integer nhits) {
		this.nhits = nhits;
	}
	public Parameters getParameters() {
		return parameters;
	}
	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}
	public List<Record> getRecords() {
		return records;
	}
	public void setRecords(List<Record> records) {
		this.records = records;
	}

	@Override
	public String toString() {
		return "DataSet [nhits=" + nhits + ", parameters=" + parameters + ", records=" + records + "]";
	}
	
}
