package com.arkon.mbus.locator.repositories;

import java.util.List;

import com.arkon.mbus.locator.documents.Record;

/**
 * Spring data custom repository
 */
public interface RecordCustomRepository {
	
	public List<Record> findByRangeTime(String since, String until);
	public List<String> findDistinctVehicles();
	public List<Integer> findAvailableMayoraltiesId();

}
