package com.arkon.mbus.locator.graph.resolvers.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arkon.mbus.locator.documents.Record;
import com.arkon.mbus.locator.services.RecordService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
public class RecordQuery implements GraphQLQueryResolver {

	@Autowired
	private RecordService recordService;
	
	/**
	 * Search all data records in the collection
	 * @return List<Record>
	 */
	public List<Record> findAllRecords() {
		return recordService.findAll();
	}

	/**
	 * Search all records from between two timestamp
	 * if dates are not provided, so search records in
	 * the last hour
	 * @return List<Record>
	 */
	public List<Record> findByRangeTime(String since, String until) {
		return recordService.findByRangeTime(since, until);
	}

	/**
	 * Search specific vehicle by id (fields.vehicle_id)
	 * @return List<Record>
	 */
	public List<Record> findByVehicleId(String id) {
		return recordService.findByVehicleId(id);
	}
	
	/**
	 * Search all availables buses in records data
	 * @return List<Record>
	 */
	public List<Record> findAvailableVehicles() {
		return recordService.findAvailableVehicles();
	}
	
	/**
	 * Search specific records by mayoralty key
	 * @param key
	 * @return List<Record>
	 */
	public List<Record> findByMayoralty(Integer key) {
		return recordService.findByMayoralty(key);
	}

}
