package com.arkon.mbus.locator.graph.resolvers.query;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;
import com.arkon.mbus.locator.services.MayoraltyService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
public class MayoraltyQuery implements GraphQLQueryResolver {

	@Autowired
	private MayoraltyService mayoraltyService;

	/**Search all data in collection
	 * @return List<Mayoralty>
	 */
	public Set<Mayoralty> findAllMayoralties() {
		return new HashSet<>(mayoraltyService.findAll());
	}
	
	/**
	 * Search all distinct mayoralties availables
	 * @return Set<Mayoralty> - list with mayoralties in desc order
	 */
	public Set<Mayoralty> findAvailableMayoralties() {
		return mayoraltyService.findAvailableMayoralties();
	}

}
