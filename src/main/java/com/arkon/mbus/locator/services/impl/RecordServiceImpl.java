package com.arkon.mbus.locator.services.impl;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arkon.mbus.locator.documents.Record;
import com.arkon.mbus.locator.repositories.RecordRepository;
import com.arkon.mbus.locator.services.RecordService;

/**
 *Service RecordService interface implementation 
 */
@Service
@Transactional
public class RecordServiceImpl implements RecordService {
	private static final Logger log = LoggerFactory.getLogger(RecordServiceImpl.class);

	@Autowired
	private RecordRepository repository;

	/**
	 * Search all data records in the collection
	 * @return List<Record>
	 */
	@Override
	public List<Record> findAll() {
		return repository.findAll();
	}

	/**
	 * Search all records from between two timestamp
	 * if dates are not provided, so search records in
	 * the last hour
	 * @return List<Record>
	 */
	@Override
	public List<Record> findByRangeTime(String since, String until) {

		if (since == null && until == null) {
			Calendar time = Calendar.getInstance();
			time.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY) - 1);
			Date prev = time.getTime();
			Date now = Calendar.getInstance().getTime();

			String pattern = "yyyy-MM-dd HH:mm:ss.SSS";
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
			since = LocalDateTime.parse(sdf.format(prev), formatter).toString();
			until = LocalDateTime.parse(sdf.format(now), formatter).toString();
			log.info("Using default range...");
		}

		return repository.findByRangeTime(since, until);
	}
	/**
	 * Search specific vehicle by id (fields.vehicle_id)
	 * @return List<Record>
	 */
	@Override
	public List<Record> findByVehicleId(String id) {
		return repository.findByVehicleId(id);
	}

	/**
	 * Search all distinct buses ids
	 * @return List<String> - string list that contains vehicles id
	 * in desc order
	 */
	@Override
	public List<String>  findDistinctVehicles() {
		return repository.findDistinctVehicles();
	}

	/**
	 * Search all availables buses in records data
	 * @return List<Record>
	 */
	@Override
	public List<Record> findAvailableVehicles() {
		List<String> availableVehicles = repository.findDistinctVehicles();
		return repository.findByListIn(availableVehicles);
	}
	
	/**
	 * Save all mbus records in mongodb
	 * @param List<Record> records
	 * @return List<Record> - saved records
	 */
	@Override
	public List<Record> saveRecords(List<Record> records) {
		return repository.saveAll(records);
	}
	
	/**
	 * Purge the records collection
	 * @return void
	 */
	@Override
	public void deleteAll() {
		repository.deleteAll();
	}

	/**
	 * Search specific records by mayoralty key
	 * @param key
	 * @return List<Record>
	 */
	@Override
	public List<Record> findByMayoralty(Integer key) {
		return repository.findByMayoralty(key);
	}

}
