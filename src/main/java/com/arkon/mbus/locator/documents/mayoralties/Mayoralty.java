package com.arkon.mbus.locator.documents.mayoralties;

import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
/**
 * Class that represent collection mayoralties
 */
@Document(collection = "mayoralties")
public class Mayoralty {

	@Indexed(name = "key_index", direction = IndexDirection.DESCENDING)
	private Integer key;
	private String name;
	
	public Mayoralty() {}
	
	public Mayoralty(Integer key, String name) {
		this.key = key;
		this.name = name;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mayoralty other = (Mayoralty) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Mayoralty [key=" + key + ", name=" + name + "]";
	}
	
}
