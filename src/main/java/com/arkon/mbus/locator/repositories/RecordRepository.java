package com.arkon.mbus.locator.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.arkon.mbus.locator.documents.Record;
/**
 * Spring data repository with multiple inheritance simulation
 * to get spring data utilities and custom functions
 */
@Repository
public interface RecordRepository extends MongoRepository<Record, String>, RecordCustomRepository {

	@Query(value="{'fields.vehicle_id': ?0}")
	public List<Record> findByVehicleId(String id);
	@Query(value="{'fields.vehicle_id': {$in:[?0]} }")
	public List<Record> findByListIn(List<String> vehicleIds);
	@Query(value="{'mayoraltyKey': ?0}")
	public List<Record> findByMayoralty(Integer key);
	public List<Record> findByRangeTime(String since, String until);
	public List<String> findDistinctVehicles();
	
}
