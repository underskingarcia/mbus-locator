package com.arkon.mbus.locator.repositories.impl;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.arkon.mbus.locator.documents.Record;
import com.arkon.mbus.locator.repositories.RecordCustomRepository;

/**
 * Spring data custom repository implementation
 */
public class RecordCustomRepositoryImpl implements RecordCustomRepository {

	@Autowired
	MongoTemplate mongoTemplate;

	private static final Logger log = LoggerFactory.getLogger(RecordCustomRepositoryImpl.class);

	/**
	 * Get all records from between two timestamp
	 * @param since - timestamp begin search
	 * @param until - timestamp end search
	 * @return List<Record>
	 */
	@Override
	public List<Record> findByRangeTime(String since, String until) {
		log.info("Query between {} and {}", since, until);
		Query query = new Query(Criteria.where("record_timestamp").gte(since).lte(until));
		List<Record> records = mongoTemplate.find(query, Record.class);
		return records;
	}
	
	/**
	 * Search all distinct buses ids
	 * @return List<String> - string list that contains vehicles id
	 * in desc order
	 */
	@Override
	public List<String> findDistinctVehicles() {
		List<String> records = mongoTemplate.query(Record.class).distinct("fields.vehicle_id").as(String.class).all();
		records = sortAsNumbers(records);
		log.info("Distinct result: {} ", records);
		return records;
	}

	/**
	 * Search all distinct mayoralties names
	 * @return List<String> - list with names in desc order
	 */
	@Override
	public List<Integer> findAvailableMayoraltiesId() {
		List<Integer> mayoralties = mongoTemplate.query(Record.class).distinct("mayoraltyKey").as(Integer.class).all();
		log.info("Distinct mayoralties: {} ", mayoralties);
		return mayoralties;
	}

	/**
	 * Sort String number list in desc order
	 * @param collection
	 * @return
	 */
	public static List<String> sortAsNumbers(Collection<String> collection) {
	    return collection
	            .stream()
	            .map(Integer::valueOf)
	            .sorted()
	            .map(String::valueOf)
	            .collect(Collectors.toList());
	}

}
