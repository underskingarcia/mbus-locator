package com.arkon.mbus.locator.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 *  Class that represent MBus embedded object records
 */
@Document(collection = "records")
public class Record {
	@Id
	private String _id; 
	private String datasetid;
	@JsonProperty("recordid")
	@Field("recordid")
	private String recordId;
	private Fields fields;
	private Geometry geometry;
	private Mayoralty mayoralty;
	@Indexed(name = "record_timestamp_index")
	@JsonProperty("record_timestamp")
	@Field("record_timestamp")
	@TextIndexed private String recordTimestamp;
	
	public Record() {}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getDatasetid() {
		return datasetid;
	}
	public void setDatasetid(String datasetid) {
		this.datasetid = datasetid;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public Fields getFields() {
		return fields;
	}
	public void setFields(Fields fields) {
		this.fields = fields;
	}
	public Geometry getGeometry() {
		return geometry;
	}
	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}
	public String getRecordTimestamp() {
		return recordTimestamp;
	}
	public Mayoralty getMayoralty() {
		return mayoralty;
	}
	public void setMayoralty(Mayoralty mayoralty) {
		this.mayoralty = mayoralty;
	}
	public void setRecordTimestamp(String recordTimestamp) {
		this.recordTimestamp = recordTimestamp;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((recordId == null) ? 0 : recordId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		if (recordId == null) {
			if (other.recordId != null)
				return false;
		} else if (!recordId.equals(other.recordId))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Records [datasetid=" + datasetid + ", recordid=" + recordId + ", fields=" + fields + ", geometry="
				+ geometry + ", recordTimestamp=" + recordTimestamp + "]";
	}
	
}
