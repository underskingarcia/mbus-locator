package com.arkon.mbus.locator.services.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.arkon.mbus.locator.documents.Record;
import com.arkon.mbus.locator.services.RecordService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class RecordServiceImplTest {

	private static final Logger LOG = LoggerFactory.getLogger(RecordServiceImplTest.class);

	@Test
	void testFindAll() throws UnsupportedEncodingException, Exception {
		List<Record> records = jsonToRecordList(getTestRecords());
		RecordService recordService = mock(RecordService.class);
		when(recordService.findAll()).thenReturn(records);
		assertNotNull(records);
		assertTrue(records.get(0) instanceof Record);
		LOG.info("Resource response testFindAll: {}", records.size());
	}

	@Test
	void testFindByRangeTime() throws UnsupportedEncodingException, Exception {
		String since = "since=2020-07-06T01:00:00.000";
		String until = "&until=2021-07-30T23:59:59.000";
		RecordService recordService = mock(RecordService.class);
		List<Record> range = jsonToRecordList(getTestRecords());
		when(recordService.findByRangeTime(since, until)).thenReturn(range);
		assertNotNull(range);
		assertTrue(range.get(0) instanceof Record);
	}

	@Test
	void testFindByRangeTimeNull() throws UnsupportedEncodingException, Exception {
		RecordService recordService = mock(RecordService.class);
		List<Record> range = jsonToRecordList(getTestRecords());
		when(recordService.findByRangeTime(null, null)).thenReturn(range);
		assertNotNull(range);
		assertTrue(range.get(0) instanceof Record);
	}

	@Test
	void testFindByVehicleId() throws UnsupportedEncodingException, Exception {
		RecordService recordService = mock(RecordService.class);
		List<Record> records = jsonToRecordList(getTestRecords());
		when(recordService.findByVehicleId("1286")).thenReturn(records);
		assertNotNull(records);
		assertTrue(records.get(0) instanceof Record);
		assertFalse(records.get(0).getFields().getVehicleId().equals("12864454"));
		assertTrue(records.get(0).getFields().getVehicleId().equals("1286"));
	}

	@Test
	void testFindAvailableVehicles() throws UnsupportedEncodingException, Exception {
		RecordService recordService = mock(RecordService.class);
		List<Record> records = jsonToRecordList(getTestRecords());
		when(recordService.findAvailableVehicles()).thenReturn(records);
		assertNotNull(records);
		assertTrue(records.get(0) instanceof Record);
		assertFalse(records.isEmpty());
	}

	@Test
	void testSaveRecords() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<Record> records = jsonToRecordList(getTestRecords());
		RecordService serviceMock = mock(RecordService.class);
		when(serviceMock.saveRecords(records)).thenReturn(records);
		List<Record> savedRecords = serviceMock.saveRecords(records);
		assertNotNull(savedRecords);
		assertTrue(savedRecords.equals(records));
		assertTrue(savedRecords.get(0) instanceof Record);
		assertFalse(savedRecords.isEmpty());
		verify(serviceMock).saveRecords(records);
	}

	@Test
	void testDeleteAll() {
		RecordService serviceMock = mock(RecordService.class);
		serviceMock.deleteAll();
		doNothing().when(serviceMock).deleteAll();
		verify(serviceMock).deleteAll();
	}

	private List<Record> jsonToRecordList(String response) throws JsonProcessingException, JsonMappingException {
		ObjectMapper jsonMapper = new ObjectMapper();
		return jsonMapper.readValue(response,
				jsonMapper.getTypeFactory().constructCollectionType(List.class, Record.class));
	}

	private String getTestRecords() throws IOException, JsonParseException, JsonMappingException {
		Path resourceDirectory = Paths.get("src","test","resources", "test_data_metrobus.json");
		String absolutePath = resourceDirectory.toFile().getAbsolutePath();
		ObjectMapper mapper = new ObjectMapper();
		File file = new File(absolutePath);
		JsonNode jsonNode = mapper.readTree(file);
		return jsonNode.toString();
	}

}
