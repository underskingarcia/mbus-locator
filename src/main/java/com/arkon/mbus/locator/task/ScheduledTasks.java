package com.arkon.mbus.locator.task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.arkon.mbus.locator.documents.Record;
import com.arkon.mbus.locator.documents.api.geocoding.LocationMap;
import com.arkon.mbus.locator.documents.api.metrobus.DataSet;
import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;
import com.arkon.mbus.locator.services.MayoraltyService;
import com.arkon.mbus.locator.services.RecordService;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * This class is a repetitive task, it is programmed to
  * run every hour with equal start delay.
  * Its function is to connect to the metrobus api and geocoding
  * api from google maps to obtain the base data for the app,
  * from the metrobus api it obtains the positioning data
  * of the vehicles and from maps take the name of the mayoralties
  * in which these were positioned*/
@Component
public class ScheduledTasks {

	private static final Logger LOG = LoggerFactory.getLogger(ScheduledTasks.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

	@Value("${mbus.endpoint}")
	private String mbusEndpoint;
	@Value("${mbus.rows}")
	private Integer mbusRows;
	@Value("${mbus.start}")
	private String mbusStart;
	@Value("${map.endpoint}")
	private String mapEndpoint;
	@Value("${map.apikey}")
	private String apikey;
	private static final String LOCATION_NOT_FOUND = "LOCATION_NOT_FOUND";
	private List<Record> recordsPaged = new LinkedList<Record>();
	private List<Mayoralty> mayoralties = null;

	@Autowired
	private RecordService recordService;
	@Autowired
	private MayoraltyService mayoraltyService;

	/*
	 * Run the task every hour
	 * Get the endpoint data and replace the current collection
	 * @throws JsonProcessingException - if parsing json to object data fail*/
	@Scheduled(fixedRate = 60*60*1000)
	public void updateMbusRecords() {
		try {
			//get all data from mbus api and geocoding
			List<Record> records = getPagedData();
			//save new records with mayoralties
			List<Record> collection = recordService.findAll();
			//Check if collected data is already in collection based in recordid field
			if(!collection.containsAll(records)) {
				recordService.deleteAll();
				recordService.saveRecords(records);
				LOG.info("Saved all records {}",  dateFormat.format(Calendar.getInstance().getTime()));
			}else {
				LOG.info("All records are up to date {}",  dateFormat.format(Calendar.getInstance().getTime()));
			}
		} catch (JsonProcessingException e) {
			LOG.error("Error procesing data from endpoint: {}", e.getMessage());
		}
	}

	/**
	 * Query Mbus api to get all records
	 * @param startRow - row number from which the query starts
	 * @return DataSet - Complete mbus data with mayoralty info 
	 * @throws JsonProcessingException
	 */
	private DataSet getMbusDataset(int startRow) throws JsonProcessingException {
		RestTemplate template = new RestTemplate();
		//start = number of first row that get
		//rows = how many rows get
		String mBusUrl = mbusEndpoint + "&start=" + startRow + "&rows=" + mbusRows;
		DataSet dataset = template.getForObject(mBusUrl, DataSet.class);
		LOG.info("Updating Data, please wait... {} Mbus Url: {}",  dateFormat.format(Calendar.getInstance().getTime()), mBusUrl);
		putMayoraltyToRecords(dataset);
		return dataset;
	}

	/**
	 * Query geocoding api and return name of mayoralty
	 * @param lat - latitude
	 * @param lng - longitude
	 * @return String - name of mayoralty
	 */
	private String getMayoraltyFromPosition(Double lat, Double lng) {
		//Create query string to query geocoding
		StringBuilder sb = new StringBuilder(mapEndpoint);
		sb.append("latlng=")
		.append(lat)
		.append(",")
		.append(lng)
		.append("&result_type=administrative_area_level_3")
		.append("&key=")
		.append(apikey);
		String urlMap = new String(sb);
//		LOG.info("Map url: {}", urlMap);
		RestTemplate template = new RestTemplate();
		LocationMap map = template.getForObject(urlMap, LocationMap.class);
		return map.getResults().get(0).getAddressComponents().get(0).getLongName();
	}

	/**
	 * Add name and key of mayoralty to mbus records
	 * based in their coordinates
	 * @param dataset
	 * @throws JsonProcessingException
	 */
	private void putMayoraltyToRecords(DataSet dataset) throws JsonProcessingException {
		//filter the records where GeographicPoint is 0.0 because map don't work with this coordinate 
		List<Record> records = dataset.getRecords().stream()
				.filter(Objects::nonNull)
				.filter(gp -> gp.getFields().getGeographicPoint() != null
						&& !gp.getFields().getGeographicPoint().isEmpty())
				.collect(Collectors.toList());
		
		//put mayoralty name and key in record objects 
		records.forEach(record -> {
			List<Double> gp = record.getFields().getGeographicPoint();
			if(record.getFields().getGeographicPoint().get(0).intValue() == 0
					|| record.getFields().getGeographicPoint().get(1).intValue() == 0) {
				record.setMayoralty(getMayoralty(LOCATION_NOT_FOUND));
			}else {
				String mayoralty = getMayoraltyFromPosition(gp.get(0), gp.get(1));
				record.setMayoralty(getMayoralty(mayoralty));
			}
		});

		LOG.info("Record with mayoralty: {}", records.size());
	}

	//@TODO convert loop to functional
	/**
	 * Query mbus api collecting data page from page until get all
	 * @return List<Record> - All records collected
	 * @throws JsonProcessingException
	 */
	public List<Record> getPagedData() throws JsonProcessingException {
		//get first page of data
		DataSet dataset = getMbusDataset(0);
		LOG.info("Page: {}", 1);
		//number of total rows
		int dataLength = dataset.getNhits();
		//add the first page of records to list
		//get number of pages to query
		Integer pages = (int) Math.ceil((float) dataLength / mbusRows);
		LOG.info("Total Pages: {}", pages);
		recordsPaged.addAll(dataset.getRecords());
		if (dataset.getNhits() > 0) {
			for (int i = 2; i <= pages; i++) {
				LOG.info("Page: {}", i);
				int fromIndex = (i - 1) * mbusRows;
				//is the last page, so get number of missing rows 
				if (i == pages) {
					mbusRows = dataLength - fromIndex;
				}
				dataset = getMbusDataset(fromIndex);
				//add the page of records to list
				recordsPaged.addAll(dataset.getRecords());
			}
			//clear original list
			dataset.getRecords().clear();
			//add full records to dataset
			dataset.getRecords().addAll(recordsPaged);
		}
		LOG.info("Total Paged Data: {}", recordsPaged.size());
		return recordsPaged;
	}
	
	/**
	 * Get mayoralty catalog from db and get corresponding
	 * data to each record
	 * @param name - name of mayoralty
	 * @return Mayoralty - Key
	 */
	public Mayoralty getMayoralty(String name) {
		//if mayoralties do new instance and search mayoralties
		//is not null, so get the list
		if(mayoralties == null) {
			mayoralties = mayoraltyService.findAll();	
		}
		//get name of mayoralty to add to record object 
		Mayoralty mayoralty = mayoralties.stream()
				  .filter(m -> name.equals(m.getName()))
				  .findAny()
				  .orElse(null);
		if(mayoralty == null) {
			LOG.info("KEY: {}", name);
			mayoralty = new Mayoralty();
			mayoralty.setKey(1);
		}
		
		return mayoralty;
	}

}
