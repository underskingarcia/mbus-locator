package com.arkon.mbus.locator.exception;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
 
@XmlRootElement(name = "error")
public class ErrorResponse  {
	
	private Date time = Calendar.getInstance().getTime();
    private String message;
    private List<String> details;
    
	public ErrorResponse(String message, List<String> details) {
        super();
        this.message = message;
        this.details = details;
    }
	
	public String getTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		return df.format(time);
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}
 
}