package com.arkon.mbus.locator.documents;

import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class that represent MBus embedded 0bject fields
 */
public class Fields {
	
	@Indexed(name = "vehicle_id_index")
	@JsonProperty("vehicle_id")
	@Field("vehicle_id")
	@TextIndexed private String vehicleId;
	@JsonProperty("trip_start_date")
	@Field("trip_start_date")
	private String tripStartDate;
	@JsonProperty("date_updated")
	@Field("date_updated")
	private String dateUpdated;
	@JsonProperty("position_longitude")
	@Field("position_longitude")
	private String positionLongitude;
	@JsonProperty("trip_schedule_relationship")
	@Field("trip_schedule_relationship")
	private String tripScheduleRelationship;
	@JsonProperty("position_speed")
	@Field("position_speed")
	private String positionSpeed;
	@JsonProperty("position_latitude")
	@Field("position_latitude")
	private String positionLatitude;
	@JsonProperty("trip_route_id")
	@Field("trip_route_id")
	private String tripRouteId;
	@JsonProperty("vehicle_label")
	@Field("vehicle_label")
	private String vehicleLabel;
	@JsonProperty("position_odometer")
	@Field("position_odometer")
	private String positionOdometer;
	@JsonProperty("trip_id")
	@Field("trip_id")
	private String tripId;
	@JsonProperty("vehicle_current_status")
	@Field("vehicle_current_status")
	private String vehicleCurrentStatus;
	@JsonProperty("geographic_point")
	@Field("geographic_point")
	private List<Double> geographicPoint;
	
	public Fields() {}
	
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getTripStartDate() {
		return tripStartDate;
	}
	public void setTripStartDate(String tripStartDate) {
		this.tripStartDate = tripStartDate;
	}
	public String getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(String dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public String getPositionLongitude() {
		return positionLongitude;
	}
	public void setPositionLongitude(String positionLongitude) {
		this.positionLongitude = positionLongitude;
	}
	public String getTripScheduleRelationship() {
		return tripScheduleRelationship;
	}
	public void setTripScheduleRelationship(String tripScheduleRelationship) {
		this.tripScheduleRelationship = tripScheduleRelationship;
	}
	public String getPositionSpeed() {
		return positionSpeed;
	}
	public void setPositionSpeed(String positionSpeed) {
		this.positionSpeed = positionSpeed;
	}
	public String getPositionLatitude() {
		return positionLatitude;
	}
	public void setPositionLatitude(String positionLatitude) {
		this.positionLatitude = positionLatitude;
	}
	public String getTripRouteId() {
		return tripRouteId;
	}
	public void setTripRouteId(String tripRouteId) {
		this.tripRouteId = tripRouteId;
	}
	public String getVehicleLabel() {
		return vehicleLabel;
	}
	public void setVehicleLabel(String vehicleLabel) {
		this.vehicleLabel = vehicleLabel;
	}
	public String getPositionOdometer() {
		return positionOdometer;
	}
	public void setPositionOdometer(String positionOdometer) {
		this.positionOdometer = positionOdometer;
	}
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public String getVehicleCurrentStatus() {
		return vehicleCurrentStatus;
	}
	public void setVehicleCurrentStatus(String vehicleCurrentStatus) {
		this.vehicleCurrentStatus = vehicleCurrentStatus;
	}
	public List<Double> getGeographicPoint() {
		return geographicPoint;
	}
	public void setGeographicPoint(List<Double> geographicPoint) {
		this.geographicPoint = geographicPoint;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vehicleId == null) ? 0 : vehicleId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fields other = (Fields) obj;
		if (vehicleId == null) {
			if (other.vehicleId != null)
				return false;
		} else if (!vehicleId.equals(other.vehicleId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Field [vehicleId=" + vehicleId + ", tripStartDate=" + tripStartDate + ", dateUpdated=" + dateUpdated
				+ ", positionLongitude=" + positionLongitude + ", tripScheduleRelationship=" + tripScheduleRelationship
				+ ", positionSpeed=" + positionSpeed + ", positionLatitude=" + positionLatitude + ", tripRouteId="
				+ tripRouteId + ", vehicleLabel=" + vehicleLabel + ", positionOdometer=" + positionOdometer
				+ ", tripId=" + tripId + ", vehicleCurrentStatus=" + vehicleCurrentStatus + ", geographicPoint="
				+ geographicPoint + "]";
	}
	
}
