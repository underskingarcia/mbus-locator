package com.arkon.mbus.locator.services;

import java.util.List;

import com.arkon.mbus.locator.documents.Record;
/**
 *Service Record interface 
 */
public interface RecordService {

	public List<Record> findAll();
	public List<Record> findByRangeTime(String since, String until);
	public List<Record> findByVehicleId(String id);
	public List<String> findDistinctVehicles();
	public List<Record> findAvailableVehicles();
	public List<Record> saveRecords(List<Record> records);
	public void deleteAll();
	public List<Record> findByMayoralty(Integer key);
	
}
