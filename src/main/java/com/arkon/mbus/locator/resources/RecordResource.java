package com.arkon.mbus.locator.resources;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arkon.mbus.locator.documents.Record;
import com.arkon.mbus.locator.services.RecordService;

/**
 *This class expose a resource to consume records collection
 */
@RestController
public class RecordResource {

	@Autowired
	private RecordService service;
	
	/**
	 * Search all records
	 * @return List<Record>
	 */
	@GetMapping("/api/records")
	public List<Record> findAll(){
		List<Record> response = new LinkedList<>();
		try {
		 response = service.findAll();
		}catch (NullPointerException e) {
		}
		return response;
	}
	
	/**
	 * Search a list of records between two timestamp
	 * @param since
	 * @param until
	 * @return List<Record>
	 */
	@GetMapping("/api/records/range")
	public List<Record> findByRangeTime(@RequestParam(required = false) String since, @RequestParam(required = false) String until){
		return service.findByRangeTime(since, until);
	}
	
	/**
	 * Search a list of each distinc vehicles
	 * @return List<String> 
	 */
	@GetMapping("/api/records/vehicle")
	public List<String>  findDistinctVehicles(){
		return service.findDistinctVehicles();
	}
	
	/**
	 * Search specific list vehicle by id
	 * @param vehicleId
	 * @return List<Record>
	 */
	@GetMapping("/api/records/vehicle/{vehicleId}")
	public List<Record> findByVehicleId(@PathVariable String vehicleId){
		return service.findByVehicleId(vehicleId);
	}
	
	/**
	 * Search list vehicle of available vehicles
	 * @param vehicleId
	 * @return List<Record>
	 */
	@GetMapping("/api/records/vehicle/available")
	public List<Record> findAvailableVehicles(){
		return service.findAvailableVehicles();
	}
	
	/**
	 * Search specific records by mayoralty key
	 * @param key
	 * @return List<Record>
	 */
	@GetMapping("/api/records/mayoralty/{key}")
	public List<Record> findByMayoralty(@PathVariable Integer key){
		return service.findByMayoralty(key);
	}
	
}
