package com.arkon.mbus.locator.documents.api.geocoding;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class that represent MBus embedded 0bject address
 */
public class Address {

	@JsonProperty("long_name")
	private String longName;
	@JsonProperty("short_name")
	private String shortName;
	private List<String> types;
	
	public Address() {}

	public String getLongName() {
		return longName;
	}

	public void setLongName(String longName) {
		this.longName = longName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(List<String> types) {
		this.types = types;
	}

	@Override
	public String toString() {
		return "Address [longName=" + longName + ", shortName=" + shortName + ", types=" + types + "]";
	}
	
}
