package com.arkon.mbus.locator.config;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.stereotype.Component;

import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;
import com.arkon.mbus.locator.repositories.MayoraltyRepository;

@Component
public class MayoraltyDataConfig implements InitializingBean {

    @Autowired
    private MappingMongoConverter mappingMongoConverter;

    @Override
    public void afterPropertiesSet() throws Exception {
        mappingMongoConverter.setTypeMapper(new DefaultMongoTypeMapper(null));
    }

	@Autowired
	private MayoraltyRepository repository;

	@PostConstruct
	public void generateData() {
		if (repository.findAll().isEmpty()) {
			List<Mayoralty> mayoralties = new LinkedList<Mayoralty>();
			mayoralties.add(new Mayoralty(1, "LOCATION_NOT_FOUND"));
			mayoralties.add(new Mayoralty(2, "Álvaro Obregón"));
			mayoralties.add(new Mayoralty(3, "Azcapotzalco"));
			mayoralties.add(new Mayoralty(4, "Benito Juárez"));
			mayoralties.add(new Mayoralty(5, "Coyoacán"));
			mayoralties.add(new Mayoralty(6, "Cuajimalpa de Morelos"));
			mayoralties.add(new Mayoralty(7, "Cuauhtémoc"));
			mayoralties.add(new Mayoralty(8, "Gustavo A. Madero"));
			mayoralties.add(new Mayoralty(9, "Iztacalco"));
			mayoralties.add(new Mayoralty(10, "Iztapalapa"));
			mayoralties.add(new Mayoralty(11, "La Magdalena Contreras"));
			mayoralties.add(new Mayoralty(12, "Miguel Hidalgo"));
			mayoralties.add(new Mayoralty(13, "Milpa Alta"));
			mayoralties.add(new Mayoralty(14, "Tláhuac"));
			mayoralties.add(new Mayoralty(15, "Tlalpan"));
			mayoralties.add(new Mayoralty(16, "Venustiano Carranza"));
			mayoralties.add(new Mayoralty(17, "Xochimilco"));
			mayoralties.add(new Mayoralty(2, "Alvaro Obregon"));
			mayoralties.add(new Mayoralty(4, "Benito Juarez"));
			mayoralties.add(new Mayoralty(5, "Coyoacan"));
			mayoralties.add(new Mayoralty(7, "Cuauhtemoc"));
			mayoralties.add(new Mayoralty(14, "Tlahuac"));
			repository.saveAll(mayoralties);
		}
	}

}
