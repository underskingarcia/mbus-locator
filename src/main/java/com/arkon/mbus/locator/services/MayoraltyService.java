package com.arkon.mbus.locator.services;

import java.util.List;
import java.util.Set;

import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;

/**
 *Service MayoraltyService interface 
 */
public interface MayoraltyService {

	public List<Mayoralty> findAll();
	public Set<Mayoralty> findAvailableMayoralties();
	
}
