package com.arkon.mbus.locator.services.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;
import com.arkon.mbus.locator.services.MayoraltyService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class MayoraltyServiceImplTest {

	private static final Logger LOG = LoggerFactory.getLogger(MayoraltyServiceImplTest.class);
	
	@Test
	void testFindAll() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		MayoraltyService mayoraltyService = mock(MayoraltyService.class);
		List<Mayoralty> findAll = jsonToList(getTestMayoralties());
		when(mayoraltyService.findAll()).thenReturn(findAll);
		assertNotNull(mayoraltyService);
		assertNotNull(findAll);
		LOG.info("Size data :{}", findAll.size());
		assertEquals(22, findAll.size());
	}
	
	@Test
	public Set<Mayoralty> testFindAvailableMayoralties() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		MayoraltyService mayoraltyService = mock(MayoraltyService.class);
		List<Mayoralty> available = jsonToList(getTestMayoralties());
		HashSet<Mayoralty> hashSet = new HashSet<>(available);
		when(mayoraltyService.findAvailableMayoralties()).thenReturn(hashSet);
		assertNotNull(mayoraltyService);
		assertNotNull(available);
		LOG.info("Size data :{}", available.size());
		assertEquals(22, available.size());
		return hashSet;
	}
	
	private List<Mayoralty> jsonToList(String response)
			throws JsonProcessingException, JsonMappingException {
		ObjectMapper jsonMapper = new ObjectMapper();
		return jsonMapper.readValue(response, jsonMapper.getTypeFactory().constructCollectionType(List.class, Mayoralty.class));
	}
	
	private String getTestMayoralties() throws IOException, JsonParseException, JsonMappingException {
		Path resourceDirectory = Paths.get("src","test","resources", "test_data_mayoralty.json");
		String absolutePath = resourceDirectory.toFile().getAbsolutePath();
		ObjectMapper mapper = new ObjectMapper();
		File file = new File(absolutePath);
		JsonNode jsonNode = mapper.readTree(file);
		return jsonNode.toString();
	}

}
