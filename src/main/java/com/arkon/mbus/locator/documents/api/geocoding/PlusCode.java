package com.arkon.mbus.locator.documents.api.geocoding;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class that represent MBus embedded 0bject pluscode
 */
public class PlusCode {
	
	@JsonProperty("compound_code")
	private String compoundCode;
	@JsonProperty("global_code")
	private String globalCode;
	
	public PlusCode() {}

	public String getCompoundCode() {
		return compoundCode;
	}

	public void setCompoundCode(String compoundCode) {
		this.compoundCode = compoundCode;
	}

	public String getGlobalCode() {
		return globalCode;
	}

	public void setGlobalCode(String globalCode) {
		this.globalCode = globalCode;
	}

	@Override
	public String toString() {
		return "PlusCode [compoundCode=" + compoundCode + ", globalCode=" + globalCode + "]";
	}

}
