package com.arkon.mbus.locator.documents.api.geocoding;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Class that represent Geocoding embedded 0bject results
 */
public class Results {
	
	@JsonProperty("address_components")
	private List<Address> addressComponents;
	
	public Results() {}

	public List<Address> getAddressComponents() {
		return addressComponents;
	}

	public void setAddressComponents(List<Address> addressComponents) {
		this.addressComponents = addressComponents;
	}

	@Override
	public String toString() {
		return "Results [addressComponents=" + addressComponents + "]";
	}
	
}
