package com.arkon.mbus.locator.documents.api.metrobus;
/**
 * Class that represent MBus embedded 0bject parameters
 */
public class Parameters {
	
	private String dataset;
	private String timezone;
	private Integer rows;
	private String format;
	
	public Parameters() {}
	
	public String getDataset() {
		return dataset;
	}
	public void setDataset(String dataset) {
		this.dataset = dataset;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public Integer getRows() {
		return rows;
	}
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	
	@Override
	public String toString() {
		return "Parameters [dataSet=" + dataset + ", timeZone=" + timezone + ", rows=" + rows + ", format=" + format
				+ "]";
	}
	
}
