FROM underskingarcia/api-mbus AS MAVEN_BUILD
MAINTAINER Raúl García
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn package
FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/mbus-0.0.1.jar /app/
ENTRYPOINT ["java", "-jar", "mbus-0.0.1.jar"]