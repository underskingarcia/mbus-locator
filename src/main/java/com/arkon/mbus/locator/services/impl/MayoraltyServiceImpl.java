package com.arkon.mbus.locator.services.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arkon.mbus.locator.documents.mayoralties.Mayoralty;
import com.arkon.mbus.locator.repositories.MayoraltyRepository;
import com.arkon.mbus.locator.repositories.RecordRepository;
import com.arkon.mbus.locator.services.MayoraltyService;

/**
 *Service Mayoralty interface implementation 
 */
@Service
public class MayoraltyServiceImpl implements MayoraltyService {

	@Autowired
	private MayoraltyRepository mayoraltyRepository;
	@Autowired
	private RecordRepository recordRepository;
	
	/**Search all data in collection
	 * @return List<Mayoralty>
	 */
	public List<Mayoralty> findAll(){
		return mayoraltyRepository.findAll();
	}
	
	/**
	 * Search all distinct mayoralties names
	 * @return List<String> - list with names in desc order
	 */
	@Override
	public Set<Mayoralty> findAvailableMayoralties() {
		List<Integer> keys = recordRepository.findAvailableMayoraltiesId();
		Set<Mayoralty> findAllMayoraltiesIn = mayoraltyRepository.findAllMayoraltiesIn(keys);
		return findAllMayoraltiesIn;
	}

}
